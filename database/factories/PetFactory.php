<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pet;
use App\Models\Type;
use Faker\Generator as Faker;

$factory->define(Pet::class, function (Faker $faker) {
    return [
        'type_id' => Type::inRandomOrder()->first()->id,
        'name' => $faker->firstName(),
        'age' => $faker->numberBetween(1, 30),
        'bio' => $faker->sentences(6, true),
    ];
});
