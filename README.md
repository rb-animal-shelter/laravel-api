# Animal Shelter - RESTful API

This project was built with [Laravel](https://github.com/laravel/laravel) version 7.27.0

## Requirements
- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Installation

Cloning the repository and run `composer update` command to initialise the project.

_**Public Directory**_

After installing Laravel, you must configure your web server's document / web root to be the public directory.

_**Database**_

Duplicate the `.env.example` file to a new file named `.env` and configure your database access.

_**Application Key**_

The next thing you should do after installing Laravel is set your application key to a random string. To set this key, run the `php artisan key:generate` command.

_**Base URL**_

The Angular SPA is by default configured to access API's requests from the following base url :  https://pets-api.dev. Consider using the following base url or change it in the SPA project's settings ([more details on readme](https://gitlab.com/rb-animal-shelter/angular-spa#laravel-api-base-url))

_**Database initialisation**_

To initialise the database, run `php artisan migrate`

_**Pets import**_

Go to `storage\app\import` folder and the `DeveloperTestData.csv` file (create `import` folder if not exists). After that, access to `https://pets-api.dev/api/import`. It will parse all the csv's datas and populate the database .
 
## Development tools
- If you want to populate the database for a devlopment usage, run the following command : `php artisan db:seed`
- [Laravel/telescope](https://github.com/laravel/telescope) is installed and configured to provide a debug assistant for the API. Go to `https//pets-api.dev/telescope` to view more details.
- You can use [Postman](https://github.com/postmanlabs) to view the JSON responses from the API.

## API's usage

The API is designed as a RESTful API who used [JSON API standard](https://jsonapi.org/) as a representation system of datas.

### Types
- Navigate to https://pets-api.dev/api/types to see all types
- Navigate to https://pets-api.dev/api/types?include=pets to view all types with there pets

### Pets
- Navigate to https://pets-api.dev/api/pets to see all types
- Navigate to https://pets-api.dev/api/pets?include=type to view all types with there pets

## Fractal

The [league/fractal](https://fractal.thephpleague.com) package is used on the API and provides a presentation and transformation layer for data output. Fractal is based on these main concepts :

- [_Resources_](https://fractal.thephpleague.com/resources) _(Resources are objects that represent data, and have knowledge of a “Transformer”, which is an object or callback that will know how to output the data.)_
- [_Transformers_](https://fractal.thephpleague.com/transformers) _(located in `App\Transformers` folder)_
- [_Serializers_](https://fractal.thephpleague.com/serializers) _(the [JsonApi](https://jsonapi.org) standard is used for the API)_
- Presenters  _(located in `App\Presenters` folder)_

## l5-repository

The [l5-repository](https://github.com/andersao/l5-repository) is used is used to abstract the data layer, making the application more flexible to maintain. The idea is to implement the [Repository pattern](https://bosnadev.com/2015/03/07/using-repository-pattern-in-laravel-5/?utm_source=prettus-l5-repository&utm_medium=readme&utm_campaign=prettus-l5-repository).