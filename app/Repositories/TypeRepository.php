<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Presenters\TypePresenter;
use App\Models\Type;

class TypeRepository extends BaseRepository {

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
    ];

    function model()
    {
        return Type::class;
    }

    public function presenter()
    {
        return TypePresenter::class;
    }
}