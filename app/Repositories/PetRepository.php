<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Presenters\PetPresenter;
use App\Models\Pet;

class PetRepository extends BaseRepository {

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type_id',
        'name',
        'age',
        'bio',
    ];

    function model()
    {
        return Pet::class;
    }

    public function presenter()
    {
        return PetPresenter::class;
    }
}