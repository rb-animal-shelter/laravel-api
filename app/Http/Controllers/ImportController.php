<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use App\Imports\TypesImport;
use App\Imports\Treatment;
use App\Models\Type;
use App\Models\Pet;

class ImportController extends Controller
{
    private $disk = 'import';
    private $file = 'DeveloperTestData.csv';

    public function synchronise()
    {
        if (!Storage::disk($this->disk)->exists($this->file)) return false;

        $rows = \Excel::toArray(new Treatment, $this->file, $this->disk)[0];
        
        if (!empty($rows)) {
            unset($rows[0]);

            $types = [];
            $pets = [];
            foreach ($rows as $row) {
                $types[]['name'] = $row[2];

                $pets[] = [
                    'name' => trim($row[0]),
                    'age' => (int) $row[1],
                    'type_id' => $row[2],
                    'bio' => $row[3],
                ];
            }

            $types = array_map("unserialize", array_unique(array_map("serialize", $types)));

            $types = Arr::pluck(
                Type::insertOnDuplicateKey($types, ['updated_at' => date('Y-m-d H:i:s')])
                ->get(['id','name'])
                ->toArray(), 
                'id', 'name'
            );

            foreach ($pets as &$pet) {
                $pet['type_id'] = $types[$pet['type_id']];
            }
            
            Pet::insert($pets);
        }
    }
}

