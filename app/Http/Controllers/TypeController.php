<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Http\Request;
use App\Repositories\TypeRepository;
use App\Transformers\TypeTransformer;
use League\Fractal;

class TypeController extends Controller
{
    /**
     * @var TypeRepository
     */
    protected $repository;

    public function __construct(TypeRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Controllers\sendResponse
     */
    public function index()
    {
        return $this->sendResponse($this->repository->all(), 'Types retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Http\Controllers\sendResponse
     */
    public function store(Request $request)
    {
        Type::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return \App\Http\Controllers\sendResponse
     */
    public function show($id)
    {
        $type = $this->repository->find($id);

        if (empty($type)) {
            return $this->sendError('Type not found');
        }

        return $this->sendResponse($type, 'Type retrieved successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $type
     * @return \App\Http\Controllers\sendResponse
     */
    public function update(Request $request, Type $type)
    {
        $type->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \App\Http\Controllers\sendResponse
     */
    public function destroy(Type $type)
    {
        $type->delete();
    }
}
