<?php

namespace App\Http\Controllers;

use App\Models\Pet;
use Illuminate\Http\Request;
use App\Repositories\PetRepository;
use App\Transformers\PetTransformer;
use League\Fractal;

class PetController extends Controller
{
    /**
     * @var PetRepository
     */
    protected $repository;

    public function __construct(PetRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Controllers\sendResponse
     */
    public function index()
    {
        return $this->sendResponse($this->repository->all(), 'Pets retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Http\Controllers\sendResponse
     */
    public function store(Request $request)
    {
        Pet::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pet  $pet
     * @return \App\Http\Controllers\sendResponse
     */
    public function show($id)
    {
        $pet = $this->repository->find($id);

        if (empty($pet)) {
            return $this->sendError('Pet not found');
        }

        return $this->sendResponse($pet, 'Pet retrieved successfully');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pet  $pet
     * @return \App\Http\Controllers\sendResponse
     */
    public function update(Request $request, Pet $pet)
    {
        $pet->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pet  $pet
     * @return \App\Http\Controllers\sendResponse
     */
    public function destroy(Pet $pet)
    {
        $pet->update($request->all());
    }
}
