<?php

namespace App\Presenters;

use App\Transformers\PetTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PetPresenter.
 *
 * @package namespace App\Presenters;
 */
class PetPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'Pet';
    protected $resourceKeyCollection = 'Pet';

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PetTransformer();
    }
}