<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use League\Fractal\ParamBag;
use App\Models\Type;
use App\Transformers\PetTransformer;

/**
 * Class TypeTransformer.
 */
class TypeTransformer extends TransformerAbstract
{
    private $validParams = ['limit', 'order'];

    /**
     * List of resources to include.
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        'pets',
    ];

    /**
     * Transform the Type entity.
     *
     * @param \App\Models\Type $model
     *
     * @return array
     */
    public function transform(Type $model, ParamBag $params = null)
    {
        return [
            'id'   => (int) $model->id,
            'name' => $model->name,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    /**
     * Include Pets
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePets(Type $type, ParamBag $params = null)
    {
        $pets = $type->pets;
        return $this->collection($pets, new PetTransformer, 'Pet');
    }
}
