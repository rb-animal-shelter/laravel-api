<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use League\Fractal\ParamBag;
use App\Models\Pet;
use App\Transformers\PetTransformer;

/**
 * Class PetTransformer.
 */
class PetTransformer extends TransformerAbstract
{
    private $validParams = ['limit', 'order'];

    /**
     * List of resources to include.
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        'type',
    ];

    /**
     * Transform the Pet entity.
     *
     * @param \App\Models\Pet $model
     *
     * @return array
     */
    public function transform(Pet $model, ParamBag $params = null)
    {
        return [
            'id'   => (int) $model->id,
            'type_id' => $model->type_id,
            'name' => $model->name,
            'age' => $model->age,
            'bio' => $model->bio,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    /**
     * Include Type
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeType(Pet $pet, ParamBag $params = null)
    {
        $type = $pet->type;
        return $this->item($type, new TypeTransformer, 'Type');
    }
}
